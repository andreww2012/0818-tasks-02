// �������� �������, ������������ � �������� ������� ����� ������� ������������������ ������������� ����� (4,4,4,5,5,5,5..), � ������������ �� �����. �������� ���������������� ���������.
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#define ARRAY_SIZE 100

int find_longest_numseq(int arr[], int size) {

	int i;
	int maxcounter = 1,
		counter = 1;

	for (i = 1; i < size; i++) {
		if (arr[i] == arr[i - 1]) {
			counter++;
		}
		else {
			if (counter > maxcounter) {
				maxcounter = counter;
			}
			counter = 1;
		}
	}

	return maxcounter;

}

int main() {

	int arr[ARRAY_SIZE];
	int i;

	srand(time(0));
	setlocale(LC_ALL, "rus");

	for (i = 0; i < ARRAY_SIZE; i++) {
		arr[i] = rand() % 3; // [0;2]
		printf("%d", arr[i]);
	}

	printf("\n������ ����� ������� ������������������ ������������� �������� � �������:\n%d\n", find_longest_numseq(arr, ARRAY_SIZE));

	system("pause");

	return 0;
}
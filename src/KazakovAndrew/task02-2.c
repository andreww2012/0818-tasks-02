// �������� �������, ���������� ������ �� ���������. ������� ������ ���������� ���� 0 ���� 1. �������� ���������������� ���������.
#include <stdio.h>
#include <string.h>
#include <locale.h>
#define STR_LENGTH 100

int is_palindrome(char str[]) {

	char newstr[STR_LENGTH];
	unsigned int i, j,
		length, counter;
	char result = 0;

	// ������� ������� ������ �� �������� (�������� ������ �� ����� ����������!)
	j = 0;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] != ' ') {
			newstr[j] = str[i];
			j++;
		}
	}
	newstr[j] = 0;

	// ���������� �������� �� ���������
	length = strlen(newstr);
	i = 0;
	counter = 0;
	while (newstr[i] == newstr[length - 1 - i] && i <= (length - 2) / 2) {
		counter++;
		i++;
	}

	if (counter == (length - 2) / 2 + 1 || length == 1) {
		result = 1;
	}

	return result;

}

int main() {

	char str[STR_LENGTH];

	setlocale(LC_ALL, "rus");

	puts("������� ��������� ������ �� �������� �� ��������� (� ������ ��������):");
	fgets(str, STR_LENGTH, stdin);
	str[strlen(str) - 1] = 0; // \n -> \0

	if (is_palindrome(str)) {
		puts("��� ���������.");
	}
	else {
		puts("��� �� ���������.");
	}

	system("pause");

	return 0;
}